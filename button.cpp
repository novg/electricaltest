#include "button.h"
#include <QRadioButton>
#include <QBoxLayout>
#include <QDebug>
#include <QLabel>

//------------------------------------------------------------------------------
Button::Button(int allQuestions, QWidget *parent) :
    QGroupBox(parent)
{
    allQuest = allQuestions;
}

//------------------------------------------------------------------------------
Button::Button(QMap<QString, bool> quest, QWidget *parent) :
    QGroupBox(parent)
{
    QVBoxLayout *vrtLayout = new QVBoxLayout;
    QMap<QString, bool>::const_iterator imap = quest.constBegin();
    while (imap != quest.constEnd()) {
        QRadioButton *radioButton = new QRadioButton;
        QHBoxLayout *wrapLayout = new QHBoxLayout;
        QLabel *wrapLabel = new QLabel;
        wrapLabel->setWordWrap(true);
        wrapLabel->setMinimumWidth(900);
        wrapLabel->setText(imap.key());
        wrapLayout->addWidget(radioButton);
        wrapLayout->addWidget(wrapLabel);
        wrapLayout->addStretch(1);
        vrtLayout->addLayout(wrapLayout);
        QMap<QString, bool> map;
        map.insert(imap.key(), imap.value());
        mapButton.insert(radioButton, map);
        ++imap;
        connect(radioButton, SIGNAL(clicked()), SIGNAL(signalClicked()));
    }

    setLayout(vrtLayout);
}

//------------------------------------------------------------------------------
Button::~Button()
{
    checkAnswer();
}

//------------------------------------------------------------------------------
void Button::checkAnswer()
{
    QMap<QString, bool> mapAnswer;
    auto bmap = mapButton.constBegin();
    static int numberRequest = 0;
    while (bmap != mapButton.constEnd()) {
        if (bmap.key()->isChecked()) {
            mapAnswer = bmap.value();
            break;
        }
        ++bmap;
    }

    auto amap = mapAnswer.constBegin();
    if (amap.value() == false) {
        emit signalFalseAnswer(amap.key(), numberRequest);
    }

    ++numberRequest;
    if (numberRequest > allQuest) {
        numberRequest = 0;
    }
}
