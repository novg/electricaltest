#ifndef BUTTON_H
#define BUTTON_H

#include <QGroupBox>
#include <QList>
#include <QMap>
class QRadioButton;

class Button : public QGroupBox
{
    Q_OBJECT
public:
    explicit Button(int allQuestions, QWidget *parent = 0);
    explicit Button(QMap<QString, bool> quest, QWidget *parent = 0);
    ~Button();

signals:
    QString signalFalseAnswer(QString answer, int numberRequest);
    void signalClicked();

public slots:

private:
    void checkAnswer();

private:
    QMap<QRadioButton*, QMap<QString, bool>> mapButton;
    int allQuest;

};

#endif // BUTTON_H
