#include "buttonwidget.h"

#include <QSignalMapper>
#include <QGridLayout>
#include <QPushButton>

//------------------------------------------------------------------------------
ButtonWidget::ButtonWidget(QStringList texts, QWidget *parent) :
    QWidget(parent)
{
    signalMapper = new QSignalMapper(this);

    QGridLayout *gridLayout = new QGridLayout;
    for (int i = 0; i < texts.size(); ++i) {
        QPushButton *button = new QPushButton(texts[i]);
        connect(button, SIGNAL(clicked()), signalMapper, SLOT(map()));
        signalMapper->setMapping(button, texts[i].toInt());
        gridLayout->addWidget(button, i / 10, i % 10);
    }

    setLayout(gridLayout);

    connect(signalMapper, SIGNAL(mapped(int)), this, SIGNAL(clicked(int)));
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
