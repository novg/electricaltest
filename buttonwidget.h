#ifndef BUTTONWIDGET_H
#define BUTTONWIDGET_H

#include <QWidget>

class QSignalMapper;

class ButtonWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ButtonWidget(QStringList texts, QWidget *parent = 0);

signals:
    void clicked(const int value);

private:
    QSignalMapper *signalMapper;

public slots:

};

#endif // BUTTONWIDGET_H
