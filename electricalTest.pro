#-------------------------------------------------
#
# Project created by QtCreator 2014-08-26T10:41:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = electrical
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    button.cpp \
    startwidget.cpp \
    resultwindow.cpp \
    requestwidget.cpp \
    buttonwidget.cpp \
    falseanswerswidget.cpp \
    randombutton.cpp

HEADERS  += mainwindow.h \
    button.h \
    startwidget.h \
    globalType.h \
    resultwindow.h \
    requestwidget.h \
    buttonwidget.h \
    falseanswerswidget.h \
    randombutton.h

FORMS    += mainwindow.ui

QMAKE_CXXFLAGS += -std=c++11

RC_FILE = glossy-3d-blue-power.rc

RESOURCES += \
    images.qrc
