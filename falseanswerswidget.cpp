#include "falseanswerswidget.h"

#include <QLabel>
#include <QBoxLayout>

//------------------------------------------------------------------------------
FalseAnswersWidget::FalseAnswersWidget(QWidget *parent) :
    QWidget(parent)
{
}

//------------------------------------------------------------------------------
FalseAnswersWidget::FalseAnswersWidget(typeFalseAnswers falseAnswers, QWidget *parent) :
    QWidget(parent)
{
    QFont questionFont("Tahoma", 11, QFont::Bold);
    QVBoxLayout *verticalLayout = new QVBoxLayout;

    if (falseAnswers.isEmpty()) {
        QLabel *labelNotErrors = new QLabel("<H2>Ошибок нет!</H2>");
        labelNotErrors->setStyleSheet("QLabel { color : green; }");
        verticalLayout->addWidget(labelNotErrors);
    } else {
        for (auto question : falseAnswers) {
            QLabel *labelQuestion       = new QLabel(question.at(1) + "\n");
            QLabel *labelFalseAnswer    = new QLabel(question.at(2) + "\n");
            QLabel *labelTrueAnswer     = new QLabel(question.at(3) + "\n\n");
            QLabel *labelYourAnswer     = new QLabel("Ваш ответ:");
            QLabel *labelCorrectAnswer  = new QLabel("Правильный ответ:");
            QLabel *labelNumberQuestion = new QLabel("Вопрос №" + question.at(0));

            labelQuestion->setMinimumWidth(900);
            labelFalseAnswer->setMinimumWidth(900);
            labelTrueAnswer->setMinimumWidth(900);

            labelQuestion->setWordWrap(true);
            labelFalseAnswer->setWordWrap(true);
            labelTrueAnswer->setWordWrap(true);

            labelCorrectAnswer->setStyleSheet("QLabel { color : green; }");
            labelTrueAnswer->setStyleSheet("QLabel { color : green; }");
            labelYourAnswer->setStyleSheet("QLabel { color : red; }");
            labelFalseAnswer->setStyleSheet("QLabel { color : red; }");

            labelQuestion->setFont(questionFont);

            labelNumberQuestion->setFont(questionFont);

            verticalLayout->addWidget(labelNumberQuestion);
            verticalLayout->addWidget(labelQuestion);

            verticalLayout->addWidget(labelYourAnswer);
            verticalLayout->addWidget(labelFalseAnswer);

            verticalLayout->addWidget(labelCorrectAnswer);
            verticalLayout->addWidget(labelTrueAnswer);
        }
    }

    setLayout(verticalLayout);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
