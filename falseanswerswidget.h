#ifndef FALSEANSWERSWIDGET_H
#define FALSEANSWERSWIDGET_H

#include "globalType.h"
#include <QWidget>

class FalseAnswersWidget : public QWidget
{
    Q_OBJECT
public:
    explicit FalseAnswersWidget(QWidget *parent = 0);
    explicit FalseAnswersWidget(typeFalseAnswers falseAnswers, QWidget *parent = 0);

signals:

public slots:

};

#endif // FALSEANSWERSWIDGET_H
