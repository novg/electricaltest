#ifndef GLOBALTYPE_H
#define GLOBALTYPE_H

#include <QList>
#include <QMap>
#include <QPair>

typedef QList<QVector<QString>> typeFalseAnswers;
typedef QMap<QString, bool> typeAnswer;
typedef QPair<QString, typeAnswer> typeQuest;

#endif // GLOBALTYPE_H
