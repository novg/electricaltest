#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    QFont newFont("Tahoma", 11);
    a.setFont(newFont);

    w.resize(1000, 500);
    w.show();

    return a.exec();
}
