#include "mainwindow.h"
//#include "ui_mainwindow.h"

#include <QLabel>
#include <QBoxLayout>
#include <QPushButton>
#include <QStackedWidget>

//------------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent) : QWidget(parent)
{
    createWidgets();
    setWindowTitle("Электробезопасность II гр.");
}

//------------------------------------------------------------------------------
MainWindow::~MainWindow()
{
}

//------------------------------------------------------------------------------
void MainWindow::slotShowQuestions()
{
    stackedWidget->setCurrentWidget(requestWidget);
}

//------------------------------------------------------------------------------
void MainWindow::slotShowResultTest(const typeFalseAnswers &, int, int)
{
    stackedWidget->setCurrentWidget(resultWidget);
}

//------------------------------------------------------------------------------
void MainWindow::slotShowStartWindow()
{
    stackedWidget->setCurrentWidget(startWidget);
}

//------------------------------------------------------------------------------
void MainWindow::createWidgets()
{
    startWidget = new StartWidget;
    requestWidget = new RequestWidget;
    resultWidget = new ResultWindow;

    stackedWidget = new QStackedWidget;
    stackedWidget->addWidget(startWidget);
    stackedWidget->addWidget(requestWidget);
    stackedWidget->addWidget(resultWidget);

    QVBoxLayout *verticalLayout = new QVBoxLayout;
    verticalLayout->addWidget(stackedWidget);
    setLayout(verticalLayout);

    connect(startWidget, SIGNAL(signalStart()), SLOT(slotShowQuestions()));
    connect(requestWidget, SIGNAL(signalEnd(typeFalseAnswers,int,int)),
            SLOT(slotShowResultTest(typeFalseAnswers,int,int)));
    connect(requestWidget, SIGNAL(signalEnd(typeFalseAnswers,int,int)),
            resultWidget, SLOT(slotShowFalseAnswers(typeFalseAnswers,int,int)));
    connect(resultWidget, SIGNAL(signalToStart()), SLOT(slotShowStartWindow()));
    connect(startWidget, SIGNAL(signalTransfer(QQueue<typeQuest>,int)),
            requestWidget, SLOT(slotShowQuestion(QQueue<typeQuest>,int)));

}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
