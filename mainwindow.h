#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "requestwidget.h"
#include "startwidget.h"
#include "resultwindow.h"

#include <QWidget>
#include <QQueue>

class QLabel;
class QPushButton;
class QStackedWidget;

namespace Ui {
class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void slotShowQuestions();
    void slotShowResultTest(const typeFalseAnswers &, int, int);
    void slotShowStartWindow();

private:
    void createWidgets();

private:
    Ui::MainWindow *ui;
    QLabel *labelHeading;
    QPushButton *buttonNext;
    QStackedWidget *stackedWidget;
    RequestWidget *requestWidget;
    StartWidget  *startWidget;
    ResultWindow *resultWidget;
};

#endif // MAINWINDOW_H
