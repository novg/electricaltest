#include "randombutton.h"

#include <QTime>

//------------------------------------------------------------------------------
RandomButton::RandomButton(QWidget *parent) :
    QPushButton(parent)
{
    connect(this, SIGNAL(clicked()), this, SLOT(slotTramserf()));
}

//------------------------------------------------------------------------------
void RandomButton::setHighValue(int high)
{
    highValue = high;
}

//------------------------------------------------------------------------------
void RandomButton::slotTramserf()
{
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());
    int randomValue = randomInt(1, highValue);

    emit signalClicked(randomValue);
}

//------------------------------------------------------------------------------
int RandomButton::randomInt(int low, int high)
{
    return qrand() % ((high + 1) - low) + low;
}
