#ifndef RANDOMBUTTON_H
#define RANDOMBUTTON_H

#include <QPushButton>

class RandomButton : public QPushButton
{
    Q_OBJECT
public:
    explicit RandomButton(QWidget *parent = 0);

signals:
    void signalClicked(int value);

public slots:

public:
    void setHighValue(int high);

private slots:
    void slotTramserf();

private:
    int randomInt(int low, int high);
    int highValue;

};

#endif // RANDOMBUTTON_H
