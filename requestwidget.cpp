#include "requestwidget.h"
#include <QLabel>
#include <QGroupBox>
#include <QBoxLayout>
#include <QPushButton>
#include <QRadioButton>
#include <QDebug>

//------------------------------------------------------------------------------
RequestWidget::RequestWidget(QWidget *parent) :
    QWidget(parent)
{
    createWidgets();
    connect(buttonNext, SIGNAL(clicked()), SLOT(slotNextQuestion()));
}

//------------------------------------------------------------------------------
void RequestWidget::slotNextQuestion()
{
    buttonNext->setDisabled(true);
    if (queueQuest.isEmpty()) {
        delete groupBox;
        createFalseAnswers();
        return;
    }

    static int numberQuestion = 1;
    quest = queueQuest.dequeue();

    delete groupBox;
    groupBox = 0;
    groupBox = new Button(quest.second);

    connect(groupBox, SIGNAL(signalClicked()), SLOT(slotButtonNextEnabled()));
    connect(groupBox, SIGNAL(signalFalseAnswer(QString, int)),
            this, SLOT(slotReceiveFalseAnswer(QString, int)));

    labelQuestion->setText(QString("%1. ").arg(numberQuestion)
                          + quest.first);

    ++numberQuestion;
    if (numberQuestion > numQuestions) {
        numberQuestion = 1;
    }
    question = quest;

    delete vertLayout;
    vertLayout = new QVBoxLayout;
    horLayout  = new QHBoxLayout;

    QLabel *labelSpacing = new QLabel;
    vertLayout->addWidget(labelHeading);
    vertLayout->addWidget(labelSpacing);
    vertLayout->addWidget(labelQuestion);
    vertLayout->addWidget(groupBox);
    vertLayout->addStretch(1);
    horLayout->addStretch(1);
    horLayout->addWidget(buttonNext);
    vertLayout->addLayout(horLayout);
    setLayout(vertLayout);
}

//------------------------------------------------------------------------------
void RequestWidget::slotButtonNextEnabled()
{
    buttonNext->setEnabled(true);
}

//------------------------------------------------------------------------------
void RequestWidget::slotShowQuestion(const QQueue<typeQuest> &queueQuestions, int numPaper)
{
    if (queueQuest.isEmpty()) {
        queueQuest = queueQuestions ;
        numQuestions = queueQuest.size();
        numberPaper = numPaper;
        labelHeading->setText(QString("<H2>Билет №%1</H2>").arg(numberPaper));
    }
    groupBox = new Button(numQuestions);
    slotNextQuestion();
}

//------------------------------------------------------------------------------
void RequestWidget::slotReceiveFalseAnswer(QString answer, int numberRequest)
{
    falseAnswers.insert(answer, numberRequest);
    listAllAnswer << question;
}

//------------------------------------------------------------------------------
void RequestWidget::createWidgets()
{
    QFont questionFont("Tahoma", 11, QFont::Bold);
    QFont headingFont("Tahoma", 12, QFont::Bold);

    vertLayout = new QVBoxLayout;
    horLayout  = new QHBoxLayout;

    labelHeading = new QLabel();
    labelHeading->setFont(headingFont);
    labelHeading->setAlignment(Qt::AlignCenter);

    labelQuestion = new QLabel();
    labelQuestion->setWordWrap(true);
    labelQuestion->setFont(questionFont);

    buttonNext = new QPushButton("&Далее");
}

//------------------------------------------------------------------------------
void RequestWidget::createFalseAnswers()
{
    typeFalseAnswers falseAnswersTransmit;

    foreach (auto rpair, listAllAnswer) {
        QVector<QString> list(4);
        auto imap = rpair.second.constBegin();
        while (imap != rpair.second.constEnd()) {
            auto map = falseAnswers.constBegin();
            while (map != falseAnswers.constEnd()) {
                if (imap.key() == map.key()) {
                    list[0] = QString("%1").arg(map.value());
                    list[1] = rpair.first;
                    list[2] = map.key();
                }
                ++map;
            }
            if (imap.value() == true) {
                list[3] = imap.key();
            }
            ++imap;
        }
        falseAnswersTransmit << list;
    }

    emit signalEnd(falseAnswersTransmit, numQuestions, numberPaper);
    falseAnswers.clear();
    listAllAnswer.clear();
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
