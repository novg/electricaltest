#ifndef ANSWERWIDGET_H
#define ANSWERWIDGET_H

#include "button.h"
#include "globalType.h"
#include <QWidget>
#include <QMap>
#include <QQueue>
#include <QBoxLayout>

class QLabel;
class QGroupBox;
class QPushButton;
class QRadioButton;
class QStackedWidget;

class RequestWidget : public QWidget
{
    Q_OBJECT
public:
    explicit RequestWidget(QWidget *parent = 0);

signals:
    typeFalseAnswers signalEnd(const typeFalseAnswers &falseAnswers,
                               int allQuestions, int numPaper);

public slots:
    void slotShowQuestion(const QQueue<typeQuest> &queueQuestions, int numPaper);

private slots:
    void slotReceiveFalseAnswer(QString answer, int numberRequest);
    void slotNextQuestion();
    void slotButtonNextEnabled();

private:
    void createWidgets();
    void createFalseAnswers();

private:
    QLabel *labelQuestion;
    QLabel *labelHeading;
    Button *groupBox;
    typeQuest quest;
    typeQuest question;
    QQueue<typeQuest> queueQuest;
    QVector<QQueue<typeQuest>> paperList;
    QList<typeQuest> listAllAnswer;
    QMap<QString, int> falseAnswers;
    QPushButton *buttonNext;
    QVBoxLayout *vrtLayout;
    QVBoxLayout *vertLayout;
    QHBoxLayout *horLayout;
    QStackedWidget *stackWidget;
    RequestWidget *answerWidget;
    int numQuestions;
    int numberPaper;
};

#endif // ANSWERWIDGET_H
