#include "resultwindow.h"

#include <QBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QScrollArea>
#include <QDebug>

//------------------------------------------------------------------------------
ResultWindow::ResultWindow(QWidget *parent) :
    QMainWindow(parent)
{
    buttonStart = new QPushButton("В &начало");
    connect(buttonStart, SIGNAL(clicked()), SIGNAL(signalToStart()));
}

//------------------------------------------------------------------------------
void ResultWindow::slotShowFalseAnswers(const typeFalseAnswers &falseAnswers,
                                        int allQuestions, int numPaper)
{
    int numberPaper = numPaper;

    QLabel *labelStart  = new QLabel("<H1>Результаты тестирования</H1>");
    labelStart->setAlignment(Qt::AlignCenter);
    QLabel *labelNumPaper = new QLabel(QString("<H3>Билет №%1</H3>")
                                       .arg(numberPaper));
    QLabel *labelResult = new QLabel(QString("<H3>Правильных ответов: %1 из %2</H3>")
                                     .arg(allQuestions - falseAnswers.size())
                                     .arg(allQuestions));

    if ((allQuestions - falseAnswers.size()) >= (allQuestions - 2)) {
        labelResult->setStyleSheet("QLabel { color : green; }");
    } else {
        labelResult->setStyleSheet("QLabel { color : red; }");
    }

    FalseAnswersWidget *falseAnswersWidget = new FalseAnswersWidget(falseAnswers);
    QWidget *centralWidget = new QWidget;

    QScrollArea *scrollArea = new QScrollArea;
    scrollArea->setAlignment(Qt::AlignHCenter);
    scrollArea->setWidget(falseAnswersWidget);

    QHBoxLayout *hLayout = new QHBoxLayout;
    hLayout->addStretch(1);
    hLayout->addWidget(buttonStart);

    QHBoxLayout *horLayout = new QHBoxLayout;
    horLayout->addWidget(labelNumPaper);
    horLayout->addStretch(1);
    horLayout->addWidget(labelResult);

    QVBoxLayout *vLayout = new QVBoxLayout;
    vLayout->addWidget(labelStart);
    vLayout->addLayout(horLayout);
    vLayout->addWidget(scrollArea);
    vLayout->addLayout(hLayout);

    centralWidget->setLayout(vLayout);
    setCentralWidget(centralWidget);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
