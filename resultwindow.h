#ifndef RESULTWINDOW_H
#define RESULTWINDOW_H

#include <QMainWindow>
#include "globalType.h"
#include "falseanswerswidget.h"

class QPushButton;
class QLabel;
class QScrollArea;

class ResultWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit ResultWindow(QWidget *parent = 0);

signals:
    void signalToStart();

public slots:
    void slotShowFalseAnswers(const typeFalseAnswers &falseAnswers,
                              int allQuestions, int numPaper);

private:
    QPushButton *buttonStart;
};

#endif // RESULTWINDOW_H
