#ifndef STARTWIDGET_H
#define STARTWIDGET_H

#include "globalType.h"
#include "buttonwidget.h"
#include "randombutton.h"
#include <QWidget>
#include <QQueue>

class QPushButton;
class QLabel;

class StartWidget : public QWidget
{
    Q_OBJECT
public:
    explicit StartWidget(QWidget *parent = 0);
    ~StartWidget();

signals:
    void signalStart();
    void signalTransfer(const QQueue<typeQuest> &questions, int numPaper);

public slots:
    void slotTransfer(int value);

private:
    void createWidgets();
    void createData();
    void createDataTwo();


private:
//    QPushButton *buttonStart;
    RandomButton *buttonStart;
    QLabel      *labelStart;
    typeQuest quest;
//    typeQuest question;
    QQueue<typeQuest> queueQuest;
    QVector<QQueue<typeQuest>> paperList;
    ButtonWidget *buttonWidget;

};

#endif // STARTWIDGET_H
